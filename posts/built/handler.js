"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateRequest3 = exports.validateRequest2 = void 0;
const postscommon_1 = require("@rwtix/postscommon");
const client_dynamodb_1 = require("@aws-sdk/client-dynamodb");
const client = new client_dynamodb_1.DynamoDBClient({ region: "ap-southeast-2" });
const command = new client_dynamodb_1.ListTablesCommand({});
const params = {
    tableName: process.env.TABLENAME
};
const validateRequest2 = async (event, context, next) => {
    const errors = "something wrong";
    if (errors) {
        return Promise.reject(errors);
    }
    try {
        return await next(event, context);
    }
    catch (err) {
        return await Promise.reject(err);
    }
};
exports.validateRequest2 = validateRequest2;
const validateRequest3 = async (event, context, next) => {
    try {
        return await next(event, context);
    }
    catch (err) {
        console.log("req3 err");
        return await Promise.reject(err);
    }
};
exports.validateRequest3 = validateRequest3;
const handler = async (event, _context) => {
    console.log("1", params);
    try {
        //const results = await client.send(command);
        // return Promise.reject(response.success(500, {}, {
        //   tables: "LabPosts",
        //   order: actualCallOrder
        // }));
        return Promise.resolve(postscommon_1.response.success(200, {}, {
            tables: "LabPosts",
        }));
    }
    catch (err) {
        return Promise.resolve(postscommon_1.response.success(500, {}, err));
    }
};
const myHandler = postscommon_1.withMiddlewares(handler, [postscommon_1.currentUser, exports.validateRequest3, postscommon_1.validateRequest]);
exports.default = myHandler;
