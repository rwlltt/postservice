import { APIGatewayProxyEvent, APIGatewayProxyResult, Context } from 'aws-lambda';
import { response, withMiddlewares, currentUser } from '@rwtix/postscommon';
import { DynamoDBClient, ListTablesCommand } from "@aws-sdk/client-dynamodb";

const client = new DynamoDBClient({ region: "ap-southeast-2" });
const command = new ListTablesCommand({});
const params = {
    tableName: process.env.TABLENAME
};

// export const validateRequest2 = async (
//   event: APIGatewayProxyEvent, 
//   context: Context, 
//   next: NextFunction
// ):Promise<APIGatewayProxyResult> => {
//   try {
//     return await next(event, context);
//   } catch (err) {
//     return await Promise.reject(err);
//   }
// };

const handler = async (
      event: APIGatewayProxyEvent, 
      _context: Context): Promise<APIGatewayProxyResult> => {
    console.log("1",params);
    try {
      //const results = await client.send(command);
      return Promise.reject(response.success(200, {}, {  
            tables: "LabPosts",
          }));
    } catch (err) {
        return Promise.resolve(response.success(500, {}, err));
    }
};

const myHandler = withMiddlewares(handler, [currentUser]);

export default myHandler;