# AWS SAM with DynamoDB

- Posts with users likes.
- User only can like the post once.

SAM template using Typescript and Layers


For new lambda functions create a new folder (see posts folder as template).

In folder of AWS lambda function posts:
  - npm run dev
 
Test is processed with Jest:
  - npm run test

To prepare for SAM build do build the package first with:
  - npm run build (it will prepare built folder for SAM build and the template.yaml )


Building the AWS deployment
The deployment has two layers: global dependencies and another small libraray example called
api-responses.

In the terminal, navigate to root foder where template.yaml located. 
  - sam build - (it is required to do this as the posts function depends on the two layers)
  - sam deploy --guided (this will deploy it to AWS cloud formation online.)

Ensure .aws-sam is in the top folder. It can now be tested local with SAM local ...:
To run the AWS stack locally:
  - sam local invoke
  - sam local start-api (as AWS API events are used in the deployed project)

